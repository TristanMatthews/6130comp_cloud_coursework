# 6130COMP_cloud_coursework

code for coursework

To change -
In mongo.js: line 22, 25, 36, 63, : change ip address to match your ip

how to run -
navigate to compose  directory, run command "sudo docker-compose up"

tests - 
1. stop a node to show that system create a node in its place
2. stop leader node to show that code elects new leader and creates new container
3. inside mongo.js set time on line 150 to suit you, tests that container creates to at an after a time of day (scheduling)
4. inside mongo.js set time on line 159 to suit you, tests that container is removed after certain time of day
