const mongoose = require('mongoose');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

systemLeader = 0

var os = require("os");
var myhostname = os.hostname();


var request = require('request');

//This is the URL endopint of your vm running docker
var url = 'http://192.168.56.15:2375';

var amqp = require('amqplib/callback_api');
amqp.connect('amqp://user:bitnami@192.168.56.15', function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {});
});

var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);
var toSend = { "name" :myhostname, "status": "alive", "nodeID": nodeID, "Leader":"no"} ;

setInterval(function() {
amqp.connect('amqp://user:bitnami@192.168.56.15', function(error0, connection) {
if (error0) {
        throw error0;
      }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              var msg =  JSON.stringify(toSend);

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });
              channel.publish(exchange, '', Buffer.from(msg));
              console.log(" [x] Sent %s", msg);
            });
            
            setTimeout(function(){
              connection.close();
            }, 500);
});
}, 1000);

var nodes = [];

amqp.connect('amqp://user:bitnami@192.168.56.15', function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });

              channel.assertQueue('', {
                        exclusive: true
                      }, function(error2, q) {
                                if (error2) {
                                            throw error2;
                                          }
                                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                                channel.bindQueue(q.queue, exchange, '');

                                channel.consume(q.queue, function(msg) {
                                      var mes = msg.content.toString();
                                      var jsonmes = JSON.parse(msg.content);
                                            if(msg.content && mes.name != myhostname) {
                                                            console.log(" [x] %s", msg.content.toString());

                                                            var flag = false;

                                                             if(nodes.length === 0){
                                                              nodes.push(jsonmes);
                                                             }else{
                                                             nodes.forEach(element => {
                                                               if(element.name == jsonmes.name){
                                                                 flag = true;
                                                               }
                                                                 });
                                                             if(!flag){
                                                                nodes.push(jsonmes);
                                                              }
                                                          }
                                                          }
                                          }, {
                                                      noAck: true
                                                    });
                              });
            });
});

setInterval(function() {
  leader = 1;
  activeNodes = 0;
  var flag= false;
  nodes.forEach(element => {
   console.log(JSON.stringify(element));
    if(element.name != myhostname){
        activeNodes++;
        if(element.nodeID > nodeID)
        {
          leader = 0;
        }
      }
      if(element.Leader == "yes"){
        flag = true;
      }
        if((leader == 1) && (activeNodes == (nodes.length - 1) && !flag))
        {
          systemLeader = 1;
          
          toSend.Leader = "yes";
        }
    });
      
  nodes = [];
}, 3000);
var numcontainers = 11;
var webcontainers = 3;
var id ="";
var check = false;
setInterval(function() {
  var hour = new Date().getUTCHours() + 1;
  if(systemLeader ==1 ){
  console.log("i am leader " + myhostname);

  //auto scaling code, runs once, creates container after 5pm, kills container after 11pm
  if(hour >= 17 && check == false){
    hostport ++;
    hostnum ++;
    var flag = true;
    check = true;
    numcontainers =12;
    webcontainers = 4;
    containerCreate(hostport, hostnum, flag);
  }else{
    if(hour >= 23 && !id == "" && hour <= 5){
      numcontainers=11;
      webcontainers = 3;
      var kill ={
        uri: url + "/v1.41/containers/"+ id +"/stop",
        method: 'POST',
        json: {}
    };
    request(kill, function (error, response) {
      if (error) {
        console.log('Error:', err);
      } else if (response.statusCode !== 200) {
        console.log('Status:', res.statusCode);
      }else{
      console.log("Containers scaled down, container killed: " + id);
      check = false;
      id="";
      }
      });
    }
  }
  containerQty();
}
}, 2000);

var hostport = 83;
var hostnum = 3;
//create the post object to send to the docker api to create a container
function containerCreate(hostport, hostnum, flag){
  var hname = "node" + hostnum;
  var create = {
    uri: url + "/v1.41/containers/create",
  method: 'POST',
    json : {"Hostname":hname,"Image":"compose_node3","ExposedPorts":{"3000/tcp":{}},"HostConfig": {"PortBindings": {"3000/tcp": [{"HostPort": hostport.toString()}]}}}};
  //send the create request
request(create, function (error, response, createBody) {
  if (!error) {
    console.log("Created container " + JSON.stringify(createBody));
    if(flag){
      id = JSON.stringify(createBody.Id);
    }
      //post object for the container start request
      var start = {
          uri: url + "/v1.40/containers/" + createBody.Id + "/start",
        method: 'POST',
        json: {}
    };
  
    //send the start request
      request(start, function (error, response, startBody) {
        if (!error) {
          console.log("Container start completed");
    
              //post object for  wait 
              var wait = {
            uri: url + "/v1.40/containers/" + createBody.Id + "/wait",
                  method: 'POST',
              json: {}
          };
     
              
        request(wait, function (error, response, waitBody ) {
            if (!error) {
              console.log("run wait complete, container will have started");
                
                      //send a simple get request for stdout from the container
                      request.get({
                          url: url + "/v1.40/containers/" + createBody.Id + "/logs?stdout=1",
                          }, (err, res, data) => {
                                  if (err) {
                                      console.log('Error:', err);
                                  } else if (res.statusCode !== 200) {
                                      console.log('Status:', res.statusCode);
                                  } else{
                                      //we need to parse the json response to access
                                      console.log("Container stdout = " + data);
                                     // containerQty();
                                  }
                              });
                      }
          });
          }
      });
  }   
});
}

function containerQty(){
  request.get({
    //we are using the /info url to get the base docker information
      url: url + "/info",
  }, (err, res, data) => {
      if (err) {
          console.log('Error:', err);
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
      } else{
        //we need to parse the json response to access
          data = JSON.parse(data)
          console.log("Number of Containers = " + data.Containers);
          console.log("Containers running =" + data.ContainersRunning);
         
          if(data.ContainersRunning < numcontainers){
            var count = 0;
            request.get({
            url: url + "/v1.24/containers/json?all=1&before=8dfafdbc3a40&size=1",
          }, (err, res, data) => {
            var arr = [];
            if (err) {
                console.log('Error:', err);
            } else if (res.statusCode !== 200) {
              console.log('Status:', res.statusCode);
            } else{
              arr = JSON.parse(data);
             arr.forEach(element => {
              if(JSON.stringify(element.Image).includes("compose_node") && JSON.stringify(element.State).includes("running")){
                count++;   
              }
             });
             if(count != 0){
            while(count < webcontainers){
              count++;
              hostport ++;
              hostnum ++;
              containerCreate(hostport, hostnum);
            }
          }
        }        
    });
    }
    }
  });
}


//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/notflixDB?replicaSet=cfgrs';

setInterval(function() {

  console.log(`Intervals are used to fire a function for the lifetime of an application.`);

}, 3000);

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var dataSchema = new Schema({
  accountId: Number,
  username: String,
  titleId: Number,
  userAction: String,
  dateTime: Date,
  pointInteraction: Number,
  typeInteraction: String
});

var dataModel = mongoose.model('Data',dataSchema, 'data');

app.get('/', (req, res) => {
  dataModel.find({},'accountId username titleId userAction dateTime pointInteraction typeInteraction', (err, data) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(data))
  })
})

app.post('/',  (req, res) => {
  var new_instance = new dataModel(req.body);
  new_instance.save(function (err) {
  if (err) res.send('Error');
  res.send(JSON.stringify(req.body))
});
})

//bind the express web service to the port specified
app.listen(port, () => {
console.log(`Express Application listening at port ` + port)
})
